
// var 1

// function addListOnPage (array, dom = document.body) {
//     let list = document.createElement('ul');
//     dom.prepend(list);

//     for (const iterator of array) {
//         let listEl = document.createElement ('li');
//         listEl.textContent = iterator;
//         list.append(listEl);
//     }
    
// }


// Var 2


function addListOnPage(array, dom = document.body) {
    const tagArray = array.map(element =>`<li>${element}</li>`);
    let listEl = tagArray.join ("");
    let list = document.createElement('ul');
    dom.prepend(list);
    list.insertAdjacentHTML('beforeend', listEl);
}

let array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

let divEl = document.createElement ('div');
document.body.prepend(divEl);

addListOnPage(array, divEl);

let array2 = ["1", "2", "3", "sea", "user", 23];
addListOnPage (array2);






