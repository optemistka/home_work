function createNewUser() {    
    //return object with last name, name and method getlogin
    const newUser = {
        firstName : prompt("Enter your First name!"),
        lastName : prompt("Enter your Last name!"),
        birthday : prompt("Enter your birthday!", "dd.mm.yyyy"),
        getLogin : function () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getPassword : function () {
            let birthD = new Date(this.birthday.slice(6), this.birthday.slice(3, 5) - 1, this.birthday.slice(0, 2));
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + birthD.getFullYear();
        },
        getAge : function () {
            let now = new Date(); //current date
            let birthD = new Date(this.birthday.slice(6), this.birthday.slice(3, 5) - 1, this.birthday.slice(0, 2));
            let bdThisYear = new Date(now.getFullYear(), birthD.getMonth(), birthD.getDate()); //Birthday this year
            //Age = current year - birth year
            let age = now.getFullYear() - birthD.getFullYear();
            //if birthday this year not yet come subtract 1 
            if (now < bdThisYear) {
                    age = age-1;
            }

            return age;
        }
    }

    return newUser;
}

let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getLogin());
console.log(newUser.getPassword());
console.log(newUser.getAge());
