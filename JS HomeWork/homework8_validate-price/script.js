const inputEl = document.querySelector ("#input-price");
inputEl.style.border = "2px solid grey";
inputEl.style.outline = "none";
inputEl.style.width = "200px";
inputEl.style.height = "20px";
inputEl.style.borderRadius = "4px";

function takeValue() {
    if (inputEl.value > 0) {
        
        const span = document.createElement ('span');
        document.body.before(span);
        span.innerText = `Текущая цена: ${inputEl.value}`;
        span.style.border = "2px solid grey";
        span.style.borderRadius = "10px";
        span.style.margin = "10px";

        const btn = document.createElement ("button");
        document.body.before(btn);
        btn.style.marginLeft = "5px";
        btn.innerText = "x";
        btn.style.border = "none";
        btn.style.borderRadius = "10px";
        btn.style.outline = "none";

        btn.addEventListener("click", () => {
            span.style.display = "none";
            btn.style.display = "none";
            inputEl.value = ""
        });

    } else {
        inputEl.style.border = "1px solid red";
        const invalidMessage = document.createElement("span");
        invalidMessage.innerText = "Please enter correct price";
        document.body.append(invalidMessage);
    }
}

inputEl.addEventListener ('focus', () => {inputEl.style.border = "2px solid green"});
inputEl.addEventListener("blur", () => { inputEl.style.border = "2px solid grey"; takeValue(); });


