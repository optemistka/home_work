const tabsArr = document.querySelectorAll(".tabs-title");
const contentElementArr = document.querySelectorAll(".tabs-content-item");
document.querySelector(".tabs").addEventListener("click", makeActive);

function makeActive(event) {
    for(let i = 0; i < tabsArr.length; i++) {
        if (tabsArr[i] === event.target){
            event.target.classList.add("active");
            contentElementArr[i].classList.add("tabs-content-active");
        } else {
            tabsArr[i].classList.remove("active");
            contentElementArr[i].classList.remove("tabs-content-active");
        }
    }
}