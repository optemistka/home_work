
//definition of constants to work with
const DefaultAge = 18;
const DefaultName = 'Name';
const FirstAge = 18;
const SecondAge = 22;

let DataEntered = false;
//get name of user
let name = DefaultName;
while (DataEntered == false) {
	name = prompt('Please enter your name', DefaultName);
	if (name != '' ){    	
    	DataEntered = true;
	}
}

//get age of the user
DataEntered = false;
let age = DefaultAge;
while (DataEntered == false) {
	age = prompt('Please enter your age', DefaultAge);
	if (age > 0 ){    	
    	DataEntered = true;
	}
}

//now we have name and age
if( age < FirstAge ){	
//younger than 18 - show message 'You are not allowed to visit this website'
	alert( 'You are not allowed to visit this website' );

}else if( age > SecondAge ){
//older than 22 show 'Welcome + name'
	alert( 'Welcome ' + name );
   
}else{
//from 18 to 22 show the window 'Are you sure you want to continue' with ok/cancel
	let result = confirm( 'Are you sure you want to continue' );
    if( result == true ){
    	alert( 'Welcome ' + name );
    }else{
    	alert( 'You are not allowed to visit this website' );    	
    }
}