function MathOperation(x, y, oper) {	
	switch(oper){
    	case "*": 
        	return x * y;
            break;
        case "/":         	
            return x / y;            
        	break;
        case "+": 
        	return x + y;
        	break;
        case "-": 
        	return x - y;
        	break;
    }
}

const DefaultVal1 = 1;
const DefaultVal2 = 1;
const DefaultOperator = '+';

let ValuesCorrect = false;
let FirstVal = DefaultVal1;
let SecondVal = DefaultVal2;
let Operator = DefaultOperator;

while(ValuesCorrect == false){
	FirstVal = +prompt("Enter first number ", DefaultVal1 );
	SecondVal = +prompt("Enter second number ", DefaultVal2 );
	Operator = prompt("Enter one of operators: (+-/*) ", DefaultOperator);
	if( !isNan(FirstVal) && 
        !isNaN(SecondVal) && 
        (Operator == "+" || Operator == "-" || Operator == "*" || Operator == "/")){
    	ValuesCorrect = true;
    }else{
    	alert("incorrect input, please try again");
    }
}

let result = MathOperation(FirstVal, SecondVal, Operator);
alert("result = " + result);


